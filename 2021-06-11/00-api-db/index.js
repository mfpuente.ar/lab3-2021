const express = require('express')
const cors = require('cors')
const app = express()
const port = 4000

// Conexion a base de datos
const db = require('./database')

app.use(express.json())
app.use(cors())

app.get('/', (req, res) => {
  console.log(req.query)
  res.send('Hola Mundo!')
})

// Crear una tarea (C)
app.post('/tareas', (req, res) => {
  console.log('POST en /tareas')
  const sql = 'INSERT INTO tareas (nombre, lista) VALUES (?,?)'
  const params = [req.body.nombre, req.body.lista]
  db.run(sql, params, function (err) {
    if (err) {
      console.error('Error al crear tarea: ' + err.message)
      res.send({mensaje: 'Error: '+ err.message})
      return
    }
    else {
      const tarea = {
        id: this.lastID,
        nombre: req.body.nombre,
        lista: req.body.lista
      }
      res.send({
        mensaje: 'ok',
        tarea: tarea
      })
    }
  })
})

// Leer todas las tareas (R)
app.get('/tareas', (req, res) => {
  console.log('GET en /tareas')
  const sql = 'SELECT * FROM tareas'
  const params = []
  db.all(sql, params, (err, result) => {
    if (err) {
      console.error('Error al leer las tareas: ' + err.message)
      res.send({mensaje: 'Error: '+ err.message})
      return
    }
    else {
      res.send({
        mensaje: 'ok',
        tareas: result
      })
    }
  })
})

// Leer una tarea
app.get('/tareas/:id', (req, res) => {
  console.log('GET en /tareas/' + req.params.id)
  const sql = 'SELECT * FROM tareas WHERE id = ?'
  const params = [req.params.id]
  db.get(sql, params, (err, result)=> {
    if (err) {
      console.error('Error al leer la tarea: ' + err.message)
      res.send({mensaje: 'Error: '+ err.message})
      return
    }
    else {
      if (result) {
        res.send({
          mensaje: 'ok',
          tarea: result
        })
      }
      else {
        res.send({
          mensaje: 'No existe'
        })
      }
    }
  })
})

// Actualizar una tarea (U)
app.put('/tareas/:id', (req, res) => {
  console.log('PUT en /tareas/' + req.params.id)
  const sql = 'UPDATE tareas SET nombre = ?, lista = ? WHERE id = ?'
  const params = [req.body.nombre, req.body.lista, req.params.id]
  db.run(sql, params, (err) => {
    if (err) {
      console.error('Error al actualizar la tarea: ' + err.message)
      res.send({mensaje: 'Error: '+ err.message})
      return
    }
    else {
      res.send({
        mensaje: 'ok',
        tarea: {
          id: req.params.id,
          nombre: req.body.nombre,
          lista: req.body.lista,
        }
      })
    }
  })
})

// Eliminar una tarea (D)
app.delete('/tareas/:id', (req, res) =>{
  console.log('DELETE en /tareas/' + req.params.id)
  const sql = 'DELETE FROM tareas WHERE id = ?'
  const params = [req.params.id]
  db.run(sql, params, (err) => {
    if (err) {
      console.error('Error al eliminar tarea: ' + err.message)
      res.send({mensaje: 'Error: '+ err.message})
      return
    }
    else {
      res.send({
        mensaje: 'ok',
      })
    }
  })
})

app.listen(port, () => {
  console.log(`API funcionando en http://localhost:${port}`)
})