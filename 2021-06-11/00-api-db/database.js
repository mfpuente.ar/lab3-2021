const sqlite3 = require('sqlite3').verbose()

let db = new sqlite3.Database('db.sqlite', (err) =>{
  if (err) {
    console.error('No se pudo conectar a la base de datos ' + err.message)
    throw err
  }
  else {
    console.log('Conectado a la base de datos')
    db.run('CREATE TABLE tareas (id INTEGER PRIMARY KEY AUTOINCREMENT, nombre TEXT, lista INTEGER)', (err)=> {
      if (err) {
        console.error('Error al crear la tabla ' + err.message)
      }
      else {
        console.log('Tabla creada exitosamente')
      }
    })
  }
})

module.exports = db