const tareas = [
  {
    id: 1,
    nombre: 'Tarea 1',
    lista: true
  },
  {
    id: 2,
    nombre: 'Tarea 2',
    lista: false
  }
]

module.exports = tareas
