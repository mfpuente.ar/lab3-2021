const express = require('express')
const app = express()
const port = 3000

const tareas = require('./datos')

app.use(express.static(__dirname + '/public'));
app.use(express.json())

app.get('/', (req, res) => {
  console.log('GET en /')
  console.log('query = ')
  console.log(req.query)
  res.send('Hola Mundo!')
})

// Crear una tarea (C)
app.post('/tareas', (req, res) => {
  console.log('POST en /tareas')
  console.log(req.body)
  // Ejercicio
  res.send('ok')
})

// Leer todas las tareas (R)
app.get('/tareas', (req, res) => {
  console.log('GET en /tareas')
  res.send(tareas)
})

// Leer una tarea
app.get('/tareas/:id', (req, res) => {
  // Ejercicio
})

// Actualizar una tarea (U)
app.put('/tareas/:id', (req, res) => {
  console.log('PUT en /tareas/' + req.params.id)
  tareas.map((tarea) => {
    const tareaAct = req.body
    if (tarea.id == req.params.id) {
      tarea.nombre = tareaAct.nombre
      tarea.lista = tareaAct.lista
    }
  })
  res.send('ok')
})

// Eliminar una tarea (D)
app.delete('/tareas/:id', (req, res) =>{
  console.log('DELETE en /tareas/' + req.params.id)
  tareas = tareas.filter(({id}) => id != req.params.id)
  res.send('ok')
})

app.listen(port, () => {
  console.log(`API funcionando en http://localhost:${port}`)
})