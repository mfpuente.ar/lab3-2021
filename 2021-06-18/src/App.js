import './App.css';
import MiComponente from './MiComponente'
import {useState} from 'react'
import Formulario from './Formulario'

function App() {
  const [contador, setContador] = useState(0)
  const texto = 'hola mundo, desde JS'

  const decrementar = () => {
    setContador(contador-1)
    console.log(contador)
  }
  
  const onAgregar = (usuario) => {
    // Enviar a la api los valores
    console.log('Desde App para crear')
    console.log('Usuario:')
    console.log(usuario)
  }
    
  const onModificar = (usuario) => {
    // Enviar a la api los valores
    console.log('Desde App para modificar')
    console.log('Usuario:')
    console.log(usuario)
  }
    
  return (
    <div className="App">
      <header className="App-header">
        <h1>{texto}</h1>
        <MiComponente mensaje={contador}/>
        <button onClick={() => {setContador(contador+1)}}>Incrementar</button>
        <button onClick={decrementar}>Decrementar</button>
        <Formulario nombre="" apellido="" onSubmit={onAgregar}/>
        <Formulario nombre="Pepe" apellido="Sanchez" onSubmit={onModificar}/>
      </header>
    </div>
  );
}

export default App;
