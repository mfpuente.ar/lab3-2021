import {useState} from 'react'

const Formulario = (props) => {
  const [nombre, setNombre] = useState(props.nombre)
  const [apellido, setApellido] = useState(props.apellido)

  const nombreOnChange = (event) => {
    setNombre(event.target.value)
  }

  /*
  const agregar = (e) => {
    e.preventDefault()
    // Enviar a la api los valores
    console.log('Nombre: ' + nombre)
    console.log('Apellido: ' + apellido)
  }
  */

  const agregar = (e) => {
    e.preventDefault()
    props.onSubmit({nombre, apellido})
  }

  return (
    <form onSubmit={agregar}>
      <div>
        <label>Nombre</label>
        <input type="text" name="nombre" id="nombre" value={nombre} onChange={nombreOnChange}/>
      </div>
      <div>
        <label>Apellido</label>
        <input type="text" name="apellido" id="apellido" value={apellido} onChange={e => setApellido(e.target.value)}/>
      </div>
      <div>
        <button type='submit'>Agregar</button>
      </div>
    </form>
  )
}

export default Formulario