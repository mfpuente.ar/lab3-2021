const Tarea = (props) => {
  const {tarea, onChange, onDelete} = props
  return (
    <li>
      <input type="checkbox" checked={tarea.lista} onChange={() => onChange(tarea)} />
      <span>{tarea.nombre}</span>
      <button onClick={()=>onDelete(tarea.id)}>X</button>
    </li>
  )
}

export default Tarea