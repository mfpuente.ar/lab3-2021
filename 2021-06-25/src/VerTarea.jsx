import { useEffect, useState } from "react"
import { useParams } from "react-router"

const VerTarea = () => {
  const { id } = useParams()
  const [tarea, setTarea] = useState({id:0, nombre:'', lista:0})

  useEffect(() => {
    fetch('http://localhost:4000/tareas/' + id).then((response) => {
      response.json().then((data) => {
        console.log(data.mensaje)
        setTarea(data.tarea)
      })
    })
  }, [id])

  return  (
    <h1>Tarea {tarea.lista} {tarea.nombre}</h1>
  )
}

export default VerTarea