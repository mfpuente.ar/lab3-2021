import { useEffect, useState } from 'react'
import Tarea from './Tarea'

const Tareas = () => {
  const [nuevaTarea, setNuevaTarea] = useState('')
  const [tareas, setTareas] = useState([])

  useEffect(() => {
    fetch('http://localhost:4000/tareas').then((response) => {
      response.json().then((data) => {
        console.log(data.mensaje)
        setTareas(data.tareas)
      })
    })
  }, [])

  const onNuevaTarea = async () => {
    // Post a API para crear tarea
    const response = await fetch('http://localhost:4000/tareas', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        nombre: nuevaTarea,
        lista: 0
      })
    })
    if (response.ok) {
      const data = await response.json()
      setTareas([...tareas, data.tarea])
    } else {
      alert('Error al agregar una tarea')
    }
  }

  const onChange = async (tarea) => {
    const response = await fetch('http://localhost:4000/tareas/' + tarea.id, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        nombre: tarea.nombre,
        lista: tarea.lista === 0 ? 1: 0
      })
    })
    if (response.ok) {
      const tareasTemp = tareas.map((t) => {
        if (t.id  === tarea.id) {
          return {
            ...tarea,
            lista: tarea.lista === 0 ? 1: 0,
          }
          /*
          return {
            id: tarea.id,
            nombre: tarea.nombre,
            lista: tarea.lista === 0 ? 1: 0,
          }
          */
        }
        return t
      })
      setTareas(tareasTemp)
    } else {
      alert('Error al agregar una tarea')
    }
  }

  const onDelete = async (id) => {
    // Pedir a la API que elimine la tarea
    const response = await fetch('http://localhost:4000/tareas/' + id, {
      method: 'DELETE'
    })

    if (response.ok) {
      const tareasTemp = tareas.filter((tarea) => tarea.id !== id)
      setTareas(tareasTemp)
    } else {
      console.log('No se pudo eliminar la tarea: ' + id)
    }
  }

  return (
    <>
      <h1>Listado de tareas</h1>
      <ul>
        {tareas.map((tarea) => <Tarea key={tarea.id} tarea={tarea} onDelete={onDelete} onChange={onChange} />)}
      </ul>
      <div>
        <label>Nueva Tarea:</label>
        <input value={nuevaTarea} onChange={(e) => setNuevaTarea(e.target.value)}/>
        <button onClick={onNuevaTarea}>Agregar</button>
      </div>
    </>
  )
}

export default Tareas