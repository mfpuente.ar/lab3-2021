const express = require('express')
const app = express()
const port = 3000

const tareas = [
  {
    id: 1,
    nombre: 'Tarea 1',
    lista: true
  },
  {
    id: 2,
    nombre: 'Tarea 2',
    lista: false
  }
]

app.use(express.json())

app.get('/', (req, res) => {
  console.log('GET en /')
  res.send('Hola Mundo!')
})

app.get('/tareas', (req, res) => {
  console.log('GET en /tareas')
  res.send(tareas)
})

app.get('/tareas/:id', (req, res) => {
  // Ejercicio
})

app.post('/tareas', (req, res) => {
  console.log('POST en /tareas')
  console.log(req.body)
  // Ejercicio
  res.send('ok')
})

app.get('/persona/:nombre/:apellido', (req, res) => {
  console.log('GET en /tareas/' + req.params.nombre + '/' + req.params.apellido)
  console.log('Nombre: ' + req.params.nombre)
  console.log('Apellido: ' + req.params.apellido)
  res.send('ok')
})

app.listen(port, () => {
  console.log(`Servicio funcionando en http://localhost:${port}`)
})