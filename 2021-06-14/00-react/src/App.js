import './App.css';
import MiComponente from './MiComponente'

function App() {
  const texto = 'hola mundo, desde JS'

  return (
    <div className="App">
      <header className="App-header">
        <h1>{texto}</h1>
        <MiComponente />
        <p>{1+2}</p>
      </header>
    </div>
  );
}

export default App;
