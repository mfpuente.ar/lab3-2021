/*
const ops = require('./operaciones')

console.log(ops.suma(34, 1))
console.log(ops.resta(34, 23))
*/

const { resta, suma } = require('./operaciones')

console.log(suma(34, 2))
console.log(resta(44,3))

// Obtener propiedades de un objeto 
let obj = {
  nombre: 'pepe',
  apellido: 'nino',
  edad: 34,
}

const { nombre, edad} = obj
console.log(nombre)
console.log(edad)